package com.marcelpallares.revoluttest.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.marcelpallares.revoluttest.data.models.ApiError
import com.marcelpallares.revoluttest.data.models.unexpectedApiError
import com.squareup.moshi.Moshi
import okhttp3.ResponseBody
import javax.inject.Inject

class Utils
@Inject constructor(private val context: Context,
                    private val moshi: Moshi
) {
    fun isConnectedToInternet(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(network)
        return capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                || capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
    }

    fun getApiErrorObj(errorBody: ResponseBody?): ApiError {
        val apiErrorAdapter = moshi.adapter(ApiError::class.java)

        errorBody?.let {
            try {
                val error = apiErrorAdapter.fromJson(it.string())
                error?.run { return error }
                return unexpectedApiError()
            } catch (err: Throwable) {
                return unexpectedApiError()
            }
        }
        return unexpectedApiError()
    }
}