package com.marcelpallares.revoluttest.utils

class Constants {
    companion object {
        const val API_REFRESH_RATE_MILLIS: Long = 1000

        const val KEY_ERROR = "error"
        const val KEY_STATUS = "status"
        const val KEY_MESSAGE = "message"
        const val KEY_CODE = "code"

        const val KEY_CURRENCY_BASE = "base"

        const val DEFAULT_BASE_CURRENCY = "EUR"
        const val DEFAULT_BASE_AMOUNT = 100f

        const val ERROR_UNEXPECTED = 500
    }
}

class ApiCalls {
    companion object {
        const val RATES = "latest"
    }
}