package com.marcelpallares.revoluttest.ui.commons.adapters

import com.marcelpallares.revoluttest.ui.commons.adapters.AdapterConstants
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewType
import java.io.Serializable

class Title() : Serializable, ViewType {
    override fun getViewType() = AdapterConstants.TITLE
}