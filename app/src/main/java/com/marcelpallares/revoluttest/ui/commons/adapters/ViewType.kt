package com.marcelpallares.revoluttest.ui.commons.adapters

interface ViewType {
    fun getViewType(): Int
}