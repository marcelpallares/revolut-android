package com.marcelpallares.revoluttest.ui.commons

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marcelpallares.revoluttest.data.models.ApiError
import com.marcelpallares.revoluttest.data.models.unexpectedApiError
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

open class BaseViewModel: ViewModel() {

    internal var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var apiError: MutableLiveData<ApiError> = MutableLiveData()
    private var noInternetError: MutableLiveData<Boolean> = MutableLiveData()

    fun apiError(): LiveData<ApiError> {
        return apiError
    }

    fun noInternetError(): LiveData<Boolean> {
        return noInternetError
    }

    internal fun onApiError(err: ApiError) {
        Timber.e(err.toString())
        apiError.postValue(err)
    }

    internal fun onUnexpectedError(e: Throwable) {
        Timber.e(e)
        apiError.postValue(unexpectedApiError())
    }

    internal fun internetError() {
        noInternetError.postValue(true)
    }

    fun apiErrorShown() {
        apiError.postValue(null)
    }

    fun noInternetErrorShown() {
        noInternetError.postValue(false)
    }

    override fun onCleared() {
        super.onCleared()
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

}