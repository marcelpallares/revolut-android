package com.marcelpallares.revoluttest.ui.commons

import android.content.Context
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.marcelpallares.revoluttest.R
import javax.inject.Inject

class ViewUtils
@Inject constructor(private val context: Context) {
    fun showNoInternet(coordinatorView: View) {
        showMessage(coordinatorView, context.getString(R.string.error_no_internet))
    }

    fun showNoInternet(context: Context) {
        showMessage(context, context.getString(R.string.error_no_internet))
    }

    fun showMessage(coordinatorView: View, msg: String) {
        Snackbar.make(coordinatorView, msg, Snackbar.LENGTH_LONG).show()
    }

    fun showMessage(context: Context, msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }
}