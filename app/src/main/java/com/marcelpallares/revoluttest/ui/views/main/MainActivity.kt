package com.marcelpallares.revoluttest.ui.views.main

import android.os.Bundle
import com.marcelpallares.revoluttest.R
import com.marcelpallares.revoluttest.ui.commons.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity: BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prepareUI()
    }

    private fun prepareUI() {
        //Coordinator
        coordinatorView = coordinatorSnackBar
    }

}