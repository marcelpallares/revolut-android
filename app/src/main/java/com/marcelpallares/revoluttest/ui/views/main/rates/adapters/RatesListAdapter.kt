package com.marcelpallares.revoluttest.ui.views.main.rates.adapters

import android.content.Context
import android.view.ViewGroup
import androidx.collection.SparseArrayCompat
import androidx.recyclerview.widget.RecyclerView
import com.marcelpallares.revoluttest.data.models.Rate
import com.marcelpallares.revoluttest.ui.commons.adapters.AdapterConstants
import com.marcelpallares.revoluttest.ui.commons.adapters.Title
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewType
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewTypeDelegateAdapter
import kotlin.collections.ArrayList

class RatesListAdapter(context: Context?, viewActions: RatesListDelegateAdapter.OnRateListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), RatesListAdapterInterface {

    internal var items: ArrayList<ViewType>
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
    private var viewTypes: ArrayList<Int> = ArrayList()

    init {
        viewTypes.add(AdapterConstants.RATES)
        viewTypes.add(AdapterConstants.TITLE)
        delegateAdapters.put(AdapterConstants.RATES, RatesListDelegateAdapter(context, this, viewActions))
        delegateAdapters.put(AdapterConstants.TITLE, RatesTitleDelegateAdapter())
        items = ArrayList()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        delegateAdapters.get(viewType)!!.onCreateViewHolder(parent)


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position))!!
            .onBindViewHolder(holder, items[position], position)
    }

    override fun getItemViewType(position: Int) = items[position].getViewType()

    fun addOrUpdateRates(rates: List<Rate>) {
        if (items.size > 0) updateRates(rates)
        else addRates(rates)
    }

    private fun addRates(rates: List<Rate>) {
        viewTypes.forEach { delegateAdapters.get(it)?.setCollectionLength(rates.size) }

        items.add(Title())
        items.addAll(rates)
        notifyDataSetChanged()
    }

    private fun updateRates(updatedRates: List<Rate>) {
        for (rate in updatedRates) {
            items.filterIsInstance<Rate>().filter { it.currencyCode == rate.currencyCode }.forEach {
                it.baseCode = rate.baseCode
                it.value = rate.value
            }
        }

        (delegateAdapters.get(AdapterConstants.RATES) as RatesListDelegateAdapter).updateEditTextsValues()
    }

    override fun getRateValue(currencyCode: String) : Float {
        return items.filterIsInstance<Rate>().first { it.currencyCode == currencyCode }.value
    }

    override fun animatePositions(rate: Rate, currentPosition: Int) {
        if (items.remove(rate)) {
            items.add(1, rate)
            notifyItemMoved(currentPosition, 1)
        }
    }

}