package com.marcelpallares.revoluttest.ui.views.main.rates.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcelpallares.revoluttest.R
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewType
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewTypeDelegateAdapter
import com.marcelpallares.revoluttest.utils.inflate

class RatesTitleDelegateAdapter(): ViewTypeDelegateAdapter {

    private var itemsLength = 0

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return TitleViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType, position: Int) {
        holder as TitleViewHolder
    }

    inner class TitleViewHolder(parent: ViewGroup): RecyclerView.ViewHolder(
        parent.inflate(R.layout.row_rates_title))

    override fun setCollectionLength(length: Int) {
        itemsLength = length
    }
}