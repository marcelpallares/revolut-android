package com.marcelpallares.revoluttest.ui.commons

import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.marcelpallares.revoluttest.di.utils.Injectable
import javax.inject.Inject

abstract class BaseFragment: Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    internal lateinit var viewModel: BaseViewModel

    internal var loadingIndicator: ProgressBar? = null

    internal fun onRequestLoaded() {
        loadingIndicator?.visibility = View.GONE
    }

    internal fun onRequestStarted() {
        loadingIndicator?.visibility = View.VISIBLE
    }

    internal fun onNoInternet() {
        onRequestLoaded()
        (activity as BaseActivity).showNoInternet()
        viewModel.noInternetErrorShown()
    }
}