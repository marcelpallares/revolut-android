package com.marcelpallares.revoluttest.ui.views.main.rates

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.marcelpallares.revoluttest.ui.commons.BaseFragment
import com.marcelpallares.revoluttest.R
import com.marcelpallares.revoluttest.data.models.ApiError
import com.marcelpallares.revoluttest.data.models.Rate
import com.marcelpallares.revoluttest.ui.views.main.MainActivity
import com.marcelpallares.revoluttest.ui.views.main.rates.adapters.RatesListAdapter
import com.marcelpallares.revoluttest.ui.views.main.rates.adapters.RatesListDelegateAdapter
import com.marcelpallares.revoluttest.utils.Constants
import com.marcelpallares.revoluttest.utils.androidLazy
import kotlinx.android.synthetic.main.fragment_rates.*

class RatesFragment: BaseFragment(), RatesListDelegateAdapter.OnRateListener {
    companion object {
        const val REFRESH_RATE = Constants.API_REFRESH_RATE_MILLIS
    }

    private val ratesAdapter by androidLazy {
        RatesListAdapter(context, this)
    }

    private val serviceCallHandler = Handler()
    private val callCode = Runnable {
        (viewModel as RatesViewModel).getRates()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rates, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loadUI()
        loadViewModel()
        getRates()
    }

    private fun getRates() {
        onRequestStarted()
        serviceCallHandler.post(callCode)
    }

    private fun onRatesResponse(rates: List<Rate>) {
        if (ratesAdapter.items.size == 0) {
            //TODO: Do not use this method
            ratesList.setItemViewCacheSize(rates.size)
        }

        onRequestLoaded()
        ratesAdapter.addOrUpdateRates(rates)
        serviceCallHandler.postDelayed(callCode, REFRESH_RATE)
    }

    private fun onRatesError(error: ApiError) {
        onRequestLoaded()
        (activity as MainActivity).showMessage(getString(R.string.error_unexpected))
        viewModel.apiErrorShown()
    }

    private fun loadUI() {
        loadingIndicator = progress
        ratesList.apply {
            setHasFixedSize(true)
            val linearLayout = LinearLayoutManager(context)
            layoutManager = linearLayout
            adapter = ratesAdapter
        }
    }

    private fun loadViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(RatesViewModel::class.java)

        (viewModel as RatesViewModel).ratesResult().observe(this,
            Observer<List<Rate>> {
                it?.run { onRatesResponse(it) }
            })

        viewModel.apiError().observe(this, Observer<ApiError>{
            it?.run { onRatesError(it) }
        })

        viewModel.noInternetError().observe(this, Observer<Boolean>{
            it?.run { if (it) onNoInternet() }
        })
    }

    override fun onRateSelected(rate: Rate) {
        (viewModel as RatesViewModel).selectedRate = rate
    }

    override fun onAmountModified(amount: Float) {
        (viewModel as RatesViewModel).currentAmount = amount
    }
}