package com.marcelpallares.revoluttest.ui.commons

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity: AppCompatActivity(), HasAndroidInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    internal lateinit var viewModel: BaseViewModel

    @Inject
    lateinit var viewUtils: ViewUtils

    internal var coordinatorView: CoordinatorLayout? = null

    internal fun onNoInternet() {
        showNoInternet()
        viewModel.noInternetErrorShown()
    }

    fun showMessage(msg: String) {
        coordinatorView?.let { viewUtils.showMessage(it, msg) }
            ?: run { viewUtils.showMessage(this, msg) }
    }

    fun showNoInternet() {
        coordinatorView?.let { viewUtils.showNoInternet(it) }
            ?: run { viewUtils.showNoInternet(this) }
    }
}