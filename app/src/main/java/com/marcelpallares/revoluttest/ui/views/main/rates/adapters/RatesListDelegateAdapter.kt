package com.marcelpallares.revoluttest.ui.views.main.rates.adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcelpallares.revoluttest.data.models.Rate
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewType
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewTypeDelegateAdapter
import com.marcelpallares.revoluttest.utils.inflate
import kotlinx.android.synthetic.main.row_rates_currency.view.*
import java.util.*
import android.widget.EditText
import com.marcelpallares.revoluttest.utils.Constants
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.view.inputmethod.InputMethodManager
import com.marcelpallares.revoluttest.R

class RatesListDelegateAdapter(private val context: Context?,
                               private val ratesListAdapter: RatesListAdapterInterface,
                               private val viewActions: OnRateListener): ViewTypeDelegateAdapter {
    companion object {
        const val BASE_AMOUNT = Constants.DEFAULT_BASE_AMOUNT
    }

    private var itemsLength = 0
    private val ratesEditTexts = HashMap<String, EditText>()
    private var currentAmount = BASE_AMOUNT

    interface OnRateListener {
        fun onRateSelected(rate: Rate)
        fun onAmountModified(amount: Float)
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            val amount = p0.toString().toFloatOrNull()
            amount?.let { updateCurrentAmount(it) }
            updateEditTextsValues()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return RateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType, position: Int) {
        holder as RateViewHolder
        holder.bind(item as Rate)
    }

    inner class RateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder(
        parent.inflate(R.layout.row_rates_currency)) {

        private val currencyImage = itemView.imageView_currency
        private val currencyCode = itemView.textView_currency
        private val currencyName = itemView.textView_currencyDesc
        private val etAmount = itemView.editText_amount

        fun bind(rate: Rate) {
            ratesEditTexts[rate.currencyCode] = etAmount
            currencyCode.text = rate.currencyCode
            currencyName.text = rate.currencyName
            currencyImage.clipToOutline = true
            updateEditTextValue(etAmount, rate.value)

            val flagResource = context?.resources?.getIdentifier(
                "flag_${rate.currencyCode.toLowerCase(Locale.ROOT)}",
                "drawable",
                context.packageName)

            flagResource?.let {
                currencyImage.setImageResource(it)
            } ?: run {
                currencyImage.setImageResource(0)
            }

            super.itemView.setOnClickListener {
                prepareEditTextForEdit(etAmount)
            }

            etAmount.setOnFocusChangeListener { _, hasFocus ->
                val amount = etAmount.text.toString().toFloatOrNull()

                if (hasFocus) {
                    etAmount.addTextChangedListener(textWatcher)
                    ratesListAdapter.animatePositions(rate, adapterPosition)
                    amount?.let { updateCurrentAmount(it) }
                    viewActions.onRateSelected(rate)
                }
                else etAmount.removeTextChangedListener(textWatcher)
            }
        }
    }

    private fun prepareEditTextForEdit(editText: EditText) {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        if (!editText.hasFocus()) {
            editText.requestFocus()
            editText.setSelection(editText.text.length)
            imm.showSoftInput(editText, SHOW_IMPLICIT)
        }
    }

    private fun updateCurrentAmount(amount: Float) {
        this.currentAmount = amount
        viewActions.onAmountModified(amount)
    }

    internal fun updateEditTextsValues() {
        for (row in ratesEditTexts) {
            updateEditTextValue(row.value, ratesListAdapter.getRateValue(row.key))
        }
    }

    private fun updateEditTextValue(etCurrency: EditText, value: Float) {
        val amount = value * currentAmount
        if (!etCurrency.hasFocus())
            etCurrency.setText(String.format(Locale.getDefault(), "%.2f", amount))
    }

    override fun setCollectionLength(length: Int) {
        itemsLength = length
    }
}