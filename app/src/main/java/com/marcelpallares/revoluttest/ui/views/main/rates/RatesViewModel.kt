package com.marcelpallares.revoluttest.ui.views.main.rates

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.marcelpallares.revoluttest.data.models.Rate
import com.marcelpallares.revoluttest.data.repository.RatesRepository
import com.marcelpallares.revoluttest.data.source.remote.NoInternetException
import com.marcelpallares.revoluttest.ui.commons.BaseViewModel
import com.marcelpallares.revoluttest.utils.Constants
import com.marcelpallares.revoluttest.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.util.*
import javax.inject.Inject

class RatesViewModel
@Inject constructor(private val ratesRepository: RatesRepository,
                    private val utils: Utils) : BaseViewModel() {
    companion object {
        const val BASE = Constants.DEFAULT_BASE_CURRENCY
        const val BASE_AMOUNT = Constants.DEFAULT_BASE_AMOUNT
    }

    var selectedRate = Rate(BASE, 1f, BASE, Currency.getInstance(BASE).displayName)
    var currentAmount = BASE_AMOUNT

    var ratesResult: MutableLiveData<List<Rate>> = MutableLiveData()
    private lateinit var disposableObserver: DisposableSingleObserver<List<Rate>>

    fun ratesResult(): LiveData<List<Rate>> {
        return ratesResult
    }

    fun getRates() {
        disposableObserver = object : DisposableSingleObserver<List<Rate>>() {
            override fun onSuccess(t: List<Rate>) {
                ratesResult.postValue(t)
            }

            override fun onError(e: Throwable) {
                when (e) {
                    is NoInternetException -> internetError()
                    else -> onRatesError(e)
                }
            }
        }

        compositeDisposable.add(disposableObserver)

        ratesRepository.getRates(selectedRate.currencyCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(disposableObserver)
    }

    private fun onRatesError(e: Throwable) {
        when (e) {
            is HttpException -> {
                onApiError(utils.getApiErrorObj(e.response()?.errorBody()))
            }
            else -> onUnexpectedError(e)
        }
    }

}