package com.marcelpallares.revoluttest.ui.views.main.rates.adapters

import com.marcelpallares.revoluttest.data.models.Rate

interface RatesListAdapterInterface {
    fun getRateValue(currencyCode: String):Float
    fun animatePositions(rate: Rate, currentPosition: Int)
}