package com.marcelpallares.revoluttest

import android.app.Application
import com.marcelpallares.revoluttest.di.utils.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class MainApplication: Application(), HasAndroidInjector {
    @Inject lateinit var androidInjector : DispatchingAndroidInjector<Any>
    override fun androidInjector() = androidInjector

    override fun onCreate() {
        super.onCreate()

        // Use Timber library for debug logs
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        // Use AppInjector class to inject app, activities and fragments automatically
        AppInjector.init(this)
    }
}