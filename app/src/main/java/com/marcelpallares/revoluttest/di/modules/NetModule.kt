package com.marcelpallares.revoluttest.di.modules

import com.marcelpallares.revoluttest.BuildConfig
import com.marcelpallares.revoluttest.data.source.remote.APIServiceHolder
import com.marcelpallares.revoluttest.data.source.remote.ApiService
import com.marcelpallares.revoluttest.data.source.remote.NetworkInterceptor
import com.marcelpallares.revoluttest.utils.Utils
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class NetModule(private val baseUrl: String) {

    @Provides
    @Singleton
    fun provideOkHttpClient(networkInterceptor: NetworkInterceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
            .addInterceptor(networkInterceptor)

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(logging)
        }

        return client.build()
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder().client(okHttpClient).baseUrl(baseUrl)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideApiServiceHolder(): APIServiceHolder = APIServiceHolder()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit, apiServiceHolder: APIServiceHolder): ApiService {
        val apiService = retrofit.create(ApiService::class.java)
        apiServiceHolder.setAPIService(apiService)
        return apiService
    }

    @Provides
    @Singleton
    fun provideNetworkInterceptor(utils: Utils): NetworkInterceptor = NetworkInterceptor(utils)

}