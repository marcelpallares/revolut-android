package com.marcelpallares.revoluttest.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.marcelpallares.revoluttest.di.utils.ViewModelKey
import com.marcelpallares.revoluttest.ui.commons.ViewModelFactory
import com.marcelpallares.revoluttest.ui.views.main.rates.RatesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(RatesViewModel::class)
    abstract fun bindRatesViewModel(ratesViewModel: RatesViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}