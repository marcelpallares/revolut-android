package com.marcelpallares.revoluttest.di.modules

import com.marcelpallares.revoluttest.ui.views.main.rates.RatesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector
    abstract fun contributeRatesFragment(): RatesFragment

}