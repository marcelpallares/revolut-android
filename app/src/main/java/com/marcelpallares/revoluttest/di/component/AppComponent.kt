package com.marcelpallares.revoluttest.di.component

import com.marcelpallares.revoluttest.MainApplication
import com.marcelpallares.revoluttest.di.modules.ActivitiesModule
import com.marcelpallares.revoluttest.di.modules.AppModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    ActivitiesModule::class
])
interface AppComponent {
    fun inject(app: MainApplication)
}