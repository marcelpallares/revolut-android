package com.marcelpallares.revoluttest.di.utils

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable