package com.marcelpallares.revoluttest.di.modules

import android.app.Application
import com.marcelpallares.revoluttest.ui.commons.ViewUtils
import com.marcelpallares.revoluttest.utils.Utils
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelsModule::class, NetModule::class])
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideUtils(moshi: Moshi): Utils = Utils(app, moshi)

    @Provides
    @Singleton
    fun provideViewUtils(): ViewUtils = ViewUtils(app)

}