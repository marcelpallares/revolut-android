package com.marcelpallares.revoluttest.data.source.remote

import androidx.annotation.Nullable

class APIServiceHolder {

    private var apiService: ApiService? = null

    @Nullable
    fun getAPIService(): ApiService? {
        return apiService
    }

    internal fun setAPIService(apiService: ApiService) {
        this.apiService = apiService
    }
}