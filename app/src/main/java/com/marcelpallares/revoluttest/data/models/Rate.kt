package com.marcelpallares.revoluttest.data.models

import com.marcelpallares.revoluttest.ui.commons.adapters.AdapterConstants
import com.marcelpallares.revoluttest.ui.commons.adapters.ViewType
import java.io.Serializable

data class Rate(

    var baseCode: String,
    var value: Float,
    val currencyCode: String,
    val currencyName: String?

) : Serializable, ViewType {
    override fun getViewType() = AdapterConstants.RATES
}