package com.marcelpallares.revoluttest.data.source.remote

import com.marcelpallares.revoluttest.utils.Utils
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class NetworkInterceptor
@Inject constructor(private val utils: Utils) : Interceptor {

    @Throws(NoInternetException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        return if (utils.isConnectedToInternet())
            chain.proceed(chain.request())
        else throw NoInternetException()
    }

}