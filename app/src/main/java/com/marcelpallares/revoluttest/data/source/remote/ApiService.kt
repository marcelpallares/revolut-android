package com.marcelpallares.revoluttest.data.source.remote

import com.marcelpallares.revoluttest.data.source.remote.responses.RatesResponse
import com.marcelpallares.revoluttest.utils.ApiCalls
import com.marcelpallares.revoluttest.utils.Constants
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET(ApiCalls.RATES)
    fun getRates(@Query(Constants.KEY_CURRENCY_BASE) currencyBase: String): Single<RatesResponse>

}