package com.marcelpallares.revoluttest.data.models

import com.marcelpallares.revoluttest.utils.Constants
import com.squareup.moshi.Json
import java.io.Serializable
import java.lang.StringBuilder

data class ApiError(

    @Json(name = Constants.KEY_ERROR)
    val details: ErrorDetails

) : Serializable {
    override fun toString(): String {
        val sb = StringBuilder()

        sb.append("Request error")
        details.status?.let { sb.append(" ").append(it) }
        details.code?.let { if(it.isNotEmpty()) sb.append(": ").append(it) }
        details.message?.let { if(it.isNotEmpty()) sb.append(" - ").append(it) }

        return sb.toString()
    }
}

data class ErrorDetails(

    @Json(name = Constants.KEY_STATUS)
    val status: Int?,

    @Json(name = Constants.KEY_MESSAGE)
    val message: String?,

    @Json(name = Constants.KEY_CODE)
    val code: String?

) : Serializable

fun unexpectedApiError(): ApiError {
    return ApiError(ErrorDetails(Constants.ERROR_UNEXPECTED, null, null))
}