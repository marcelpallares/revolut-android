package com.marcelpallares.revoluttest.data.repository

import com.marcelpallares.revoluttest.data.models.Rate
import com.marcelpallares.revoluttest.data.source.remote.ApiService
import com.marcelpallares.revoluttest.data.source.remote.responses.RatesResponse
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

class RatesRepository
@Inject constructor(private val apiService: ApiService) {

    fun getRates(currencyBase: String): Single<List<Rate>> {
        return apiService.getRates(currencyBase)
            .flatMap { response: RatesResponse -> mapResponseToRates(response) }
    }

    private fun mapResponseToRates(response: RatesResponse): Single<List<Rate>> {
        val ratesList: MutableList<Rate> = mutableListOf()

        //Add the baseCode currency at the beginning
        ratesList.add(Rate(response.base, 1f, response.base,
            Currency.getInstance(response.base).displayName))

        //Add all the currencies to the list of rates
        for ((currencyCode, value) in response.rates) {
            ratesList.add(Rate(response.base, value, currencyCode,
                Currency.getInstance(currencyCode).displayName))
        }

        return Single.just(ratesList)
    }
}