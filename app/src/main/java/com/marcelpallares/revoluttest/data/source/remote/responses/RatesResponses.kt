package com.marcelpallares.revoluttest.data.source.remote.responses

data class RatesResponse(
    val base: String,
    val date: String,
    val rates: Map<String, Float>
)